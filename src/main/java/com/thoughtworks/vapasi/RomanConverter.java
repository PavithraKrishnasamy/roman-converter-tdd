package com.thoughtworks.vapasi;

public class RomanConverter {

    public Integer convertRomanToArabicNumber(String roman) {
        if(roman.equals("I"))
            return 1;
        else if(roman.equals("II"))
            return 2;
        else if(roman.equals("III"))
            return 3;
        else {
            return null;
        }
    }
}
