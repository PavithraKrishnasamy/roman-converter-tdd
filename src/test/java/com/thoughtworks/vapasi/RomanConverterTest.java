package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RomanConverterTest {

    private RomanConverter romanConvertor;

    @BeforeEach
    void setUp() {

        romanConvertor = new RomanConverter();
    }

    @Test
    void shouldConvertI() {
        assertEquals(1, romanConvertor.convertRomanToArabicNumber("I"));
    }

    @Test
    void shouldConvertII() {
        assertEquals(2, romanConvertor.convertRomanToArabicNumber("II"));
    }

    @Test
    void shouldConvertIII() {
        assertEquals(3, romanConvertor.convertRomanToArabicNumber("III"));
    }

    @Test
    void shouldConvertIV() {
    }

    @Test
    void shouldConvertV() {
    }

    @Test
    void shouldConvertVI() {
    }

    @Test
    void shouldConvertVII() {
    }

    @Test
    void shouldConvertIX() {
    }

    @Test
    void shouldConvertX() {
    }

    @Test
    void shouldConvertXXXVI() {
    }

    @Test
    void shouldConvertMMXII() {
    }

    @Test
    void shouldConvertMCMXCVI() {
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {
    }
}